/**
 * FLOTANTE sumar() {
 * FLOTANTE a = 3.6;
 * SI (a > 5) { //< > <= >= <> ==
 * IMPRIMIR(a);
 * } SINO {
 * IMPRIMIR("Ni idea");
 * }
 * RETORNO a;
 * }
 */
// package sintactico;

import sintactico.Sintax;
import java.io.StringReader;
import java_cup.runtime.Symbol;

/**
 *
 * @author Maria Encalada
 */
public class analizadorSintactico {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        String cadena = "FLOTANTE sumar(){" +
                "FLOTANTE a = 3.6; " +
                "SI(a>5){" +
                "IMPRIMIR(a);" +
                "} SINO{" +
                "IMPRIMIR('NI IDEA');} " +
                "RETORNO a;  }";

        Sintax s = new Sintax(new LexerCup(new StringReader(cadena)));
        // Symbol auxS = null;
        // s.syntax_error(s);
        try {
            s.parse();
        } catch (Exception e) {

            Symbol sym = s.getS();
            // System.out.println(sym);
            System.out.println(
                    "Error de sintaxis linea" + (sym.right + 1) + " columna " + (sym.left + 1) + " Texto " + sym.value);
        }
    }
}
