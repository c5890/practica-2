
//----------------------------------------------------
// The following code was generated by CUP v0.11a beta 20060608
// Tue Jan 25 22:01:12 ECT 2022
//----------------------------------------------------

package sintactico;

import java_cup.runtime.Symbol;

/**
 * CUP v0.11a beta 20060608 generated parser.
 * 
 * @version Tue Jan 25 22:01:12 ECT 2022
 */
public class Sintax extends java_cup.runtime.lr_parser {

  /** Default constructor. */
  public Sintax() {
    super();
  }

  /** Constructor which sets the default scanner. */
  public Sintax(java_cup.runtime.Scanner s) {
    super(s);
  }

  /** Constructor which sets the default scanner. */
  public Sintax(java_cup.runtime.Scanner s, java_cup.runtime.SymbolFactory sf) {
    super(s, sf);
  }

  /** Production table. */
  protected static final short _production_table[][] = unpackFromStrings(new String[] {
      "\000\015\000\002\002\004\000\002\002\012\000\002\002" +
          "\012\000\002\003\003\000\002\003\004\000\002\004\005" +
          "\000\002\004\007\000\002\004\005\000\002\004\005\000" +
          "\002\005\017\000\002\005\017\000\002\005\030\000\002" +
          "\005\014" });

  /** Access to production table. */
  public short[][] production_table() {
    return _production_table;
  }

  /** Parse-action table. */
  protected static final short[][] _action_table = unpackFromStrings(new String[] {
      "\000\105\000\006\005\006\020\004\001\002\000\004\016" +
          "\101\001\002\000\004\002\100\001\002\000\004\016\007" +
          "\001\002\000\004\006\010\001\002\000\004\007\011\001" +
          "\002\000\004\010\012\001\002\000\010\020\016\023\014" +
          "\025\017\001\002\000\014\020\ufffe\022\ufffe\023\ufffe\024" +
          "\ufffe\025\ufffe\001\002\000\004\016\076\001\002\000\014" +
          "\020\016\022\031\023\014\024\032\025\017\001\002\000" +
          "\004\016\022\001\002\000\004\016\020\001\002\000\004" +
          "\017\021\001\002\000\016\011\ufffa\020\ufffa\022\ufffa\023" +
          "\ufffa\024\ufffa\025\ufffa\001\002\000\006\012\024\017\023" +
          "\001\002\000\016\011\ufffc\020\ufffc\022\ufffc\023\ufffc\024" +
          "\ufffc\025\ufffc\001\002\000\004\026\025\001\002\000\004" +
          "\017\026\001\002\000\016\011\ufffb\020\ufffb\022\ufffb\023" +
          "\ufffb\024\ufffb\025\ufffb\001\002\000\014\020\ufffd\022\ufffd" +
          "\023\ufffd\024\ufffd\025\ufffd\001\002\000\004\011\075\001" +
          "\002\000\004\006\044\001\002\000\004\010\033\001\002" +
          "\000\004\021\034\001\002\000\004\006\035\001\002\000" +
          "\004\004\036\001\002\000\004\025\037\001\002\000\004" +
          "\004\040\001\002\000\004\007\041\001\002\000\004\017" +
          "\042\001\002\000\004\011\043\001\002\000\004\011\ufff5" +
          "\001\002\000\004\016\045\001\002\000\004\015\046\001" +
          "\002\000\004\026\047\001\002\000\004\007\050\001\002" +
          "\000\004\010\051\001\002\000\004\021\052\001\002\000" +
          "\004\006\053\001\002\000\006\016\054\025\055\001\002" +
          "\000\004\007\061\001\002\000\004\007\056\001\002\000" +
          "\004\017\057\001\002\000\004\011\060\001\002\000\004" +
          "\011\ufff7\001\002\000\004\017\062\001\002\000\004\011" +
          "\063\001\002\000\006\011\ufff8\024\064\001\002\000\004" +
          "\010\065\001\002\000\004\021\066\001\002\000\004\006" +
          "\067\001\002\000\004\027\070\001\002\000\004\007\071" +
          "\001\002\000\004\017\072\001\002\000\004\011\073\001" +
          "\002\000\010\020\016\023\014\025\017\001\002\000\004" +
          "\011\ufff6\001\002\000\004\002\000\001\002\000\004\017" +
          "\077\001\002\000\016\011\ufff9\020\ufff9\022\ufff9\023\ufff9" +
          "\024\ufff9\025\ufff9\001\002\000\004\002\001\001\002\000" +
          "\004\006\102\001\002\000\004\007\103\001\002\000\004" +
          "\010\104\001\002\000\010\020\016\023\014\025\017\001" +
          "\002\000\014\020\016\022\031\023\014\024\032\025\017" +
          "\001\002\000\004\011\107\001\002\000\004\002\uffff\001" +
          "\002" });

  /** Access to parse-action table. */
  public short[][] action_table() {
    return _action_table;
  }

  /** <code>reduce_goto</code> table. */
  protected static final short[][] _reduce_table = unpackFromStrings(new String[] {
      "\000\105\000\004\002\004\001\001\000\002\001\001\000" +
          "\002\001\001\000\002\001\001\000\002\001\001\000\002" +
          "\001\001\000\002\001\001\000\006\003\014\004\012\001" +
          "\001\000\002\001\001\000\002\001\001\000\006\004\026" +
          "\005\027\001\001\000\002\001\001\000\002\001\001\000" +
          "\002\001\001\000\002\001\001\000\002\001\001\000\002" +
          "\001\001\000\002\001\001\000\002\001\001\000\002\001" +
          "\001\000\002\001\001\000\002\001\001\000\002\001\001" +
          "\000\002\001\001\000\002\001\001\000\002\001\001\000" +
          "\002\001\001\000\002\001\001\000\002\001\001\000\002" +
          "\001\001\000\002\001\001\000\002\001\001\000\002\001" +
          "\001\000\002\001\001\000\002\001\001\000\002\001\001" +
          "\000\002\001\001\000\002\001\001\000\002\001\001\000" +
          "\002\001\001\000\002\001\001\000\002\001\001\000\002" +
          "\001\001\000\002\001\001\000\002\001\001\000\002\001" +
          "\001\000\002\001\001\000\002\001\001\000\002\001\001" +
          "\000\002\001\001\000\002\001\001\000\002\001\001\000" +
          "\002\001\001\000\002\001\001\000\002\001\001\000\002" +
          "\001\001\000\004\004\073\001\001\000\002\001\001\000" +
          "\002\001\001\000\002\001\001\000\002\001\001\000\002" +
          "\001\001\000\002\001\001\000\002\001\001\000\002\001" +
          "\001\000\006\003\104\004\012\001\001\000\006\004\026" +
          "\005\105\001\001\000\002\001\001\000\002\001\001" });

  /** Access to <code>reduce_goto</code> table. */
  public short[][] reduce_table() {
    return _reduce_table;
  }

  /** Instance of action encapsulation class. */
  protected CUP$Sintax$actions action_obj;

  /** Action encapsulation object initializer. */
  protected void init_actions() {
    action_obj = new CUP$Sintax$actions(this);
  }

  /** Invoke a user supplied parse action. */
  public java_cup.runtime.Symbol do_action(
      int act_num,
      java_cup.runtime.lr_parser parser,
      java.util.Stack stack,
      int top)
      throws java.lang.Exception {
    /* call code in generated class */
    return action_obj.CUP$Sintax$do_action(act_num, parser, stack, top);
  }

  /** Indicates start state. */
  public int start_state() {
    return 0;
  }

  /** Indicates start production. */
  public int start_production() {
    return 0;
  }

  /** <code>EOF</code> Symbol index. */
  public int EOF_sym() {
    return 0;
  }

  /** <code>error</code> Symbol index. */
  public int error_sym() {
    return 1;
  }

  private Symbol s;

  public void syntax_error(Symbol s) {
    this.s = s;
  }

  public Symbol getS() {
    return this.s;
  }

}

/** Cup generated class to encapsulate user supplied action code. */
class CUP$Sintax$actions {
  private final Sintax parser;

  /** Constructor */
  CUP$Sintax$actions(Sintax parser) {
    this.parser = parser;
  }

  /** Method with the actual generated action code. */
  public final java_cup.runtime.Symbol CUP$Sintax$do_action(
      int CUP$Sintax$act_num,
      java_cup.runtime.lr_parser CUP$Sintax$parser,
      java.util.Stack CUP$Sintax$stack,
      int CUP$Sintax$top)
      throws java.lang.Exception {
    /* Symbol object for return from actions */
    java_cup.runtime.Symbol CUP$Sintax$result;

    /* select the action based on the action number */
    switch (CUP$Sintax$act_num) {
      /* . . . . . . . . . . . . . . . . . . . . */
      case 12: // CONDICION ::= SINO Llave_a IMPRIMIR Parentesis_a Comillas CADENA Comillas
               // Parentesis_c Punto_coma Llave_c
      {
        Object RESULT = null;

        CUP$Sintax$result = parser.getSymbolFactory().newSymbol("CONDICION", 3,
            ((java_cup.runtime.Symbol) CUP$Sintax$stack.elementAt(CUP$Sintax$top - 9)),
            ((java_cup.runtime.Symbol) CUP$Sintax$stack.peek()), RESULT);
      }
        return CUP$Sintax$result;

      /* . . . . . . . . . . . . . . . . . . . . */
      case 11: // CONDICION ::= SI Parentesis_a Identificador Op_relacional Decimal
               // Parentesis_c Llave_a IMPRIMIR Parentesis_a Identificador Parentesis_c
               // Punto_coma Llave_c SINO Llave_a IMPRIMIR Parentesis_a Texto Parentesis_c
               // Punto_coma Llave_c DECLARACION
      {
        Object RESULT = null;

        CUP$Sintax$result = parser.getSymbolFactory().newSymbol("CONDICION", 3,
            ((java_cup.runtime.Symbol) CUP$Sintax$stack.elementAt(CUP$Sintax$top - 21)),
            ((java_cup.runtime.Symbol) CUP$Sintax$stack.peek()), RESULT);
      }
        return CUP$Sintax$result;

      /* . . . . . . . . . . . . . . . . . . . . */
      case 10: // CONDICION ::= SI Parentesis_a Identificador Op_relacional Decimal
               // Parentesis_c Llave_a IMPRIMIR Parentesis_a CADENA Parentesis_c Punto_coma
               // Llave_c
      {
        Object RESULT = null;

        CUP$Sintax$result = parser.getSymbolFactory().newSymbol("CONDICION", 3,
            ((java_cup.runtime.Symbol) CUP$Sintax$stack.elementAt(CUP$Sintax$top - 12)),
            ((java_cup.runtime.Symbol) CUP$Sintax$stack.peek()), RESULT);
      }
        return CUP$Sintax$result;

      /* . . . . . . . . . . . . . . . . . . . . */
      case 9: // CONDICION ::= SI Parentesis_a Identificador Op_relacional Decimal
              // Parentesis_c Llave_a IMPRIMIR Parentesis_a Identificador Parentesis_c
              // Punto_coma Llave_c
      {
        Object RESULT = null;

        CUP$Sintax$result = parser.getSymbolFactory().newSymbol("CONDICION", 3,
            ((java_cup.runtime.Symbol) CUP$Sintax$stack.elementAt(CUP$Sintax$top - 12)),
            ((java_cup.runtime.Symbol) CUP$Sintax$stack.peek()), RESULT);
      }
        return CUP$Sintax$result;

      /* . . . . . . . . . . . . . . . . . . . . */
      case 8: // DECLARACION ::= RETORNO Identificador Punto_coma
      {
        Object RESULT = null;

        CUP$Sintax$result = parser.getSymbolFactory().newSymbol("DECLARACION", 2,
            ((java_cup.runtime.Symbol) CUP$Sintax$stack.elementAt(CUP$Sintax$top - 2)),
            ((java_cup.runtime.Symbol) CUP$Sintax$stack.peek()), RESULT);
      }
        return CUP$Sintax$result;

      /* . . . . . . . . . . . . . . . . . . . . */
      case 7: // DECLARACION ::= CADENA Identificador Punto_coma
      {
        Object RESULT = null;

        CUP$Sintax$result = parser.getSymbolFactory().newSymbol("DECLARACION", 2,
            ((java_cup.runtime.Symbol) CUP$Sintax$stack.elementAt(CUP$Sintax$top - 2)),
            ((java_cup.runtime.Symbol) CUP$Sintax$stack.peek()), RESULT);
      }
        return CUP$Sintax$result;

      /* . . . . . . . . . . . . . . . . . . . . */
      case 6: // DECLARACION ::= FLOTANTE Identificador Igual Decimal Punto_coma
      {
        Object RESULT = null;

        CUP$Sintax$result = parser.getSymbolFactory().newSymbol("DECLARACION", 2,
            ((java_cup.runtime.Symbol) CUP$Sintax$stack.elementAt(CUP$Sintax$top - 4)),
            ((java_cup.runtime.Symbol) CUP$Sintax$stack.peek()), RESULT);
      }
        return CUP$Sintax$result;

      /* . . . . . . . . . . . . . . . . . . . . */
      case 5: // DECLARACION ::= FLOTANTE Identificador Punto_coma
      {
        Object RESULT = null;

        CUP$Sintax$result = parser.getSymbolFactory().newSymbol("DECLARACION", 2,
            ((java_cup.runtime.Symbol) CUP$Sintax$stack.elementAt(CUP$Sintax$top - 2)),
            ((java_cup.runtime.Symbol) CUP$Sintax$stack.peek()), RESULT);
      }
        return CUP$Sintax$result;

      /* . . . . . . . . . . . . . . . . . . . . */
      case 4: // SENTENCIA ::= SENTENCIA DECLARACION
      {
        Object RESULT = null;

        CUP$Sintax$result = parser.getSymbolFactory().newSymbol("SENTENCIA", 1,
            ((java_cup.runtime.Symbol) CUP$Sintax$stack.elementAt(CUP$Sintax$top - 1)),
            ((java_cup.runtime.Symbol) CUP$Sintax$stack.peek()), RESULT);
      }
        return CUP$Sintax$result;

      /* . . . . . . . . . . . . . . . . . . . . */
      case 3: // SENTENCIA ::= DECLARACION
      {
        Object RESULT = null;

        CUP$Sintax$result = parser.getSymbolFactory().newSymbol("SENTENCIA", 1,
            ((java_cup.runtime.Symbol) CUP$Sintax$stack.peek()), ((java_cup.runtime.Symbol) CUP$Sintax$stack.peek()),
            RESULT);
      }
        return CUP$Sintax$result;

      /* . . . . . . . . . . . . . . . . . . . . */
      case 2: // INICIO ::= FLOTANTE Identificador Parentesis_a Parentesis_c Llave_a SENTENCIA
              // CONDICION Llave_c
      {
        Object RESULT = null;

        CUP$Sintax$result = parser.getSymbolFactory().newSymbol("INICIO", 0,
            ((java_cup.runtime.Symbol) CUP$Sintax$stack.elementAt(CUP$Sintax$top - 7)),
            ((java_cup.runtime.Symbol) CUP$Sintax$stack.peek()), RESULT);
      }
        return CUP$Sintax$result;

      /* . . . . . . . . . . . . . . . . . . . . */
      case 1: // INICIO ::= VACIO Identificador Parentesis_a Parentesis_c Llave_a SENTENCIA
              // CONDICION Llave_c
      {
        Object RESULT = null;

        CUP$Sintax$result = parser.getSymbolFactory().newSymbol("INICIO", 0,
            ((java_cup.runtime.Symbol) CUP$Sintax$stack.elementAt(CUP$Sintax$top - 7)),
            ((java_cup.runtime.Symbol) CUP$Sintax$stack.peek()), RESULT);
      }
        return CUP$Sintax$result;

      /* . . . . . . . . . . . . . . . . . . . . */
      case 0: // $START ::= INICIO EOF
      {
        Object RESULT = null;
        int start_valleft = ((java_cup.runtime.Symbol) CUP$Sintax$stack.elementAt(CUP$Sintax$top - 1)).left;
        int start_valright = ((java_cup.runtime.Symbol) CUP$Sintax$stack.elementAt(CUP$Sintax$top - 1)).right;
        Object start_val = (Object) ((java_cup.runtime.Symbol) CUP$Sintax$stack.elementAt(CUP$Sintax$top - 1)).value;
        RESULT = start_val;
        CUP$Sintax$result = parser.getSymbolFactory().newSymbol("$START", 0,
            ((java_cup.runtime.Symbol) CUP$Sintax$stack.elementAt(CUP$Sintax$top - 1)),
            ((java_cup.runtime.Symbol) CUP$Sintax$stack.peek()), RESULT);
      }
        /* ACCEPT */
        CUP$Sintax$parser.done_parsing();
        return CUP$Sintax$result;

      /* . . . . . . */
      default:
        throw new Exception(
            "Invalid action number found in internal parse table");

    }
  }
}
