package sintacticotrabajo;
import java_cup.runtime.Symbol;
%%
%class LexerCup
%type java_cup.runtime.Symbol
%cup
%full
%line
%char
L=[a-zA-Z_]+
T=[a-zA-Z_ \r\t]+
D=[0-9]+("."[  |0-9]+)?
espacio=[ ,\t,\r,\n]+
%{
    private Symbol symbol (int type, Object value) {
        return new Symbol (type,yyline,yycolumn,value);
    }
    private Symbol symbol (int type) {
        return new Symbol (type,yyline,yycolumn);
    }
%}
%%

/* Espacios en blanco */
{espacio} {/*Ignore*/}

/* Comentarios */
("//"(.)*) {/*Ignore*/}

/* Comillas */
("\"") {return new Symbol(sym.Comillas, (int) yychar, yyline, yytext());}

/* Tipos de datos */
( byte | char | long | float | double ) {return new Symbol(sym.T_dato, yychar, yyline, yytext());}

/* Tipo de dato Float (Para el main) */
(FLOTANTE) {return new Symbol(sym.FLOTANTE, yychar, yyline, yytext());}

/* IMPRIMIR */
(IMPRIMIR) {return new Symbol(sym.IMPRIMIR, yychar, yyline, yytext());}

/* RETORNO */
(RETORNO) {return new Symbol(sym.RETORNO, yychar, yyline, yytext());}

/* Tipo de dato NO RETORNO */
(VACIO) {return new Symbol (sym.VACIO, yychar, yyline, yytext());}

/* Palabra reservada If */
(SI) {return new Symbol(sym.SI, yychar, yyline, yytext());}

/* Palabra reservada Else */
(SINO) {return new Symbol(sym.SINO, yychar, yyline, yytext());}

/*Tipo de Dato Cadena de Texto*/
(CADENA) {return new Symbol(sym.CADENA, (int) yychar, yyline, yytext());}

/*Operador Igual*/
("=") {return new Symbol(sym.Igual, (int) yychar, yyline, yytext());}

/*Operador Suma*/
("+") {return new Symbol(sym.Suma, (int) yychar, yyline, yytext());}

/*Operador Resta*/
("-") {return new Symbol(sym.Resta, (int) yychar, yyline, yytext());}

/*Operador Multiplicacion*/
("*") {return new Symbol(sym.Multiplicacion, (int) yychar, yyline, yytext());}

/*Operador Division*/
("/") {return new Symbol(sym.Division, (int) yychar, yyline, yytext());}

/*Parentesis de Apertura*/
("(") {return new Symbol(sym.Parentesis_a, (int) yychar, yyline, yytext());}

/*Parentesis de Cierre*/
(")") {return new Symbol(sym.Parentesis_c, (int) yychar, yyline, yytext());}

/*Llave de Apertura*/
("{") {return new Symbol(sym.Llave_a, (int) yychar, yyline, yytext());}

/*Llave de Cierre*/
("}") {return new Symbol(sym.Llave_c, (int) yychar, yyline, yytext());}

/*Punto y coma*/
(";") {return new Symbol(sym.Punto_coma, (int) yychar, yyline, yytext());}

/*Operadores Relacionales */
( ">" | "<" | "==" | "!=" | ">=" | "<=" | "<<" | ">>" ) {return new Symbol(sym.Op_relacional, yychar, yyline, yytext());}

/* Operadores Atribucion */
( "+=" | "-="  | "*=" | "/=" | "%=" | "=" ) {return new Symbol(sym.Op_atribucion, yychar, yyline, yytext());}

/* Identificador */
{L}({L}|{D})* {return new Symbol (sym.Identificador, yychar, yyline, yytext());}

/* texto */
"\'"{T}({T}|{D})"\'"* {return new Symbol (sym.Texto, yychar, yyline, yytext());}

/*Decimal*/
("(-"{D}+")")|{D}+ {return new Symbol(sym.Decimal, (int) yychar, yyline, yytext());}

/* Error de analisis */
 . {return new Symbol(sym.ERROR, yychar, yyline, yytext());}